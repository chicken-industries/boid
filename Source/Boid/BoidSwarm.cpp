#include "BoidSwarm.h"

#include "Runtime/Core/Public/Async/ParallelFor.h"

ABoidSwarm::ABoidSwarm()
{
	PrimaryActorTick.bCanEverTick = true;

	OuterLimitBox = CreateDefaultSubobject<UBoxComponent>(FName("Outer Collision Mesh"));
	OuterLimitBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = OuterLimitBox;

	InnerLimitBox = CreateDefaultSubobject<UBoxComponent>(FName("Inner Collision Mesh"));
	InnerLimitBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	InnerLimitBox->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	BoidISMC = CreateDefaultSubobject<UInstancedStaticMeshComponent>(FName("BoidISMC"));
	BoidISMC->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	SpawnStartingInstances();
	UpdateVisualBoxes();
}

void ABoidSwarm::PostActorCreated()
{
	Super::PostActorCreated();
	SpawnStartingInstances();
}

void ABoidSwarm::PostLoad()
{
	Super::PostLoad();
	SpawnStartingInstances();
}

void ABoidSwarm::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CountScatteringTime(DeltaTime);
	CalcBoidVelocities(DeltaTime);
	MoveBoids(DeltaTime);
}

void ABoidSwarm::PrintFullSwarmInfo(FString const& Header)
{
	UE_LOG(LogTemp, Warning, TEXT("Full swarm info: %s"), *Header);
	UE_LOG(LogTemp, Warning, TEXT("count = %i"), Boids.Num());

	for (FBoid const& Boid : Boids)
	{
		UE_LOG(LogTemp, Warning, TEXT("Index: %i"), Boid.Index);
		UE_LOG(LogTemp, Warning, TEXT("Location: %f, %f, %f"), Boid.Location.X, Boid.Location.Y, Boid.Location.Z);
		UE_LOG(LogTemp, Warning, TEXT("Velocity: %f, %f, %f"), Boid.Velocity.X, Boid.Velocity.Y, Boid.Velocity.Z);
	}
}

#if WITH_EDITOR
void ABoidSwarm::PostEditChangeProperty(FPropertyChangedEvent& Event)
{
	Super::PostEditChangeProperty(Event);

	FName MemberPropertyName = Event.MemberProperty ? Event.MemberProperty->GetFName() : NAME_None;

	if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(ABoidSwarm, StartingCount))
		SpawnStartingInstances();
	else if (MemberPropertyName == GET_MEMBER_NAME_CHECKED(ABoidSwarm, OuterLimitBoxSize) ||
			 MemberPropertyName == GET_MEMBER_NAME_CHECKED(ABoidSwarm, InnerLimitBoxSize))
		UpdateVisualBoxes();
}
#endif

void ABoidSwarm::SpawnStartingInstances()
{
	Boids.Empty();
	Boids.Reserve(StartingCount);
	BoidISMC->ClearInstances();

	for (int i = 0; i != StartingCount; ++i)
	{
		FVector RandomLocation = OuterLimitBoxSize.GetMin() * RandomStream.GetUnitVector();
		FVector RandomVelocity = RandomStream.FRandRange(MinSpeed, MaxSpeed) * RandomStream.GetUnitVector();

		Boids.Add(FBoid(i, RandomLocation, RandomVelocity));

		BoidISMC->AddInstance(FTransform(RandomVelocity.Rotation(), RandomLocation, FVector(1)));
	}

	BoidISMC->SetStaticMesh(BoidStaticMesh);
}

void ABoidSwarm::CalcBoidVelocities(float DeltaTime)
{
	ParallelFor(Boids.Num(), [&](int32 i) {
		FBoid& Boid = Boids[i];

		FVector PerceivedSwarmCenter, PerceivedAvgVelocity, AvoidOthersDirection;
		CalcPerceivedSwarmStats(Boid, PerceivedSwarmCenter, PerceivedAvgVelocity, AvoidOthersDirection);

		FVector CohesionForce = (PerceivedSwarmCenter - Boid.Location);
		CohesionForce.Normalize();
		CohesionForce *= (SwarmIsScattering ? ScatteringWeight : CohesionWeight);

		FVector SeparationForce = AvoidOthersDirection;
		SeparationForce.Normalize();
		SeparationForce *= SeparationWeight;

		FVector AlignmentForce = (PerceivedAvgVelocity - Boid.Velocity);
		AlignmentForce.Normalize();
		AlignmentForce *= AlignmentWeight;

		FVector HoldInBoundsForce = GetVectorFacingIntoBoxIfOutside(Boid) * StayInBoxWeight;
		FVector HoldOutOfInnerBoundsForce = GetVectorFacingOutOfBoxIfInside(Boid) * StayOutInnerBoxWeight;

		FVector Acceleration =
			CohesionForce + SeparationForce + AlignmentForce + HoldInBoundsForce + HoldOutOfInnerBoundsForce;

		FVector Velocity = Boid.Velocity + Acceleration * DeltaTime;

		float Speed = Velocity.Size();
		FVector VelocityDirection = Velocity / Speed;

		float GravitationFactor = 1.f - GravitationImpact * VelocityDirection.Z;
		Speed *= GravitationFactor;

		Boid.FutureVelocity = VelocityDirection * FMath::Clamp(Speed, MinSpeed, MaxSpeed);
	});
}

void ABoidSwarm::MoveBoids(float DeltaTime)
{
	for (FBoid& Boid : Boids)
	{
		Boid.ApplyFutureValues(DeltaTime);
		Boid.UpdateInstancedMeshComponent(BoidISMC, (Boid == Boids.Last()));
	}
}

inline float CalcAngleBetweenVectors(FVector A, FVector B)
{
	return FMath::Acos(FVector::DotProduct(A, B)) / PI * 180;
}

void ABoidSwarm::CalcPerceivedSwarmStats(
	FBoid const& ProtagonistBoid, FVector& SwarmCenter, FVector& SwarmAvgVelocity, FVector& KeepDistanceDirection)
{
	FVector LocationAverage(0), VelocityAverage(0), FaceAwayFromOthers(0);
	int NumberPerceivedOthers = 0;

	for (FBoid const& OtherBoid : Boids)
	{
		if (ProtagonistBoid == OtherBoid)
			continue;

		FVector DifferenceLocation = OtherBoid.Location - ProtagonistBoid.Location;
		float Distance = DifferenceLocation.Size();

		if (Distance < PerceptionRadius && CalcAngleBetweenVectors(DifferenceLocation, ProtagonistBoid.Velocity) < 120)
		{
			LocationAverage += OtherBoid.Location;
			VelocityAverage += OtherBoid.Velocity;
			NumberPerceivedOthers++;
			if (NumberPerceivedOthers > 10)	 // seems to save some performance and does not visibly influence swarm behaviour
				break;
		}

		if (Distance < AvoidanceRadius)
			FaceAwayFromOthers -=
				DifferenceLocation * Distance;	   // multiply by distance so the impact is of magnitude distance^2
	}

	NumberPerceivedOthers = FMath::Max(NumberPerceivedOthers, 1);

	SwarmCenter = LocationAverage / NumberPerceivedOthers;
	SwarmAvgVelocity = VelocityAverage / NumberPerceivedOthers;
	KeepDistanceDirection = FaceAwayFromOthers;
}

FVector ABoidSwarm::GetVectorFacingIntoBoxIfOutside(FBoid const& Boid)
{
	FVector const& LocAbs = Boid.Location.GetAbs();
	FVector const& LocSign = Boid.Location.GetSignVector();

	FVector IntoBox(LocAbs.X > OuterLimitBoxSize.X ? -LocSign.X : 0,
		LocAbs.Y > OuterLimitBoxSize.Y ? -LocSign.Y : 0, LocAbs.Z > OuterLimitBoxSize.Z ? -LocSign.Z : 0);

	IntoBox.Normalize();
	return IntoBox;
}

FVector ABoidSwarm::GetVectorFacingOutOfBoxIfInside(FBoid const& Boid)
{
	FVector const LocAbs = Boid.Location.GetAbs();

	return (LocAbs.X < InnerLimitBoxSize.X && LocAbs.Y < InnerLimitBoxSize.Y && LocAbs.Z < InnerLimitBoxSize.Z)
			   ? Boid.Location / Boid.Location.Size()
			   : FVector(0);
}

void ABoidSwarm::UpdateVisualBoxes()
{
	OuterLimitBox->InitBoxExtent(OuterLimitBoxSize);
	InnerLimitBox->InitBoxExtent(InnerLimitBoxSize);
}

void ABoidSwarm::ScatterSwarm(float Duration)
{
	SwarmIsScattering = true;
	ScatteringTimer = Duration;
}

void ABoidSwarm::CountScatteringTime(float DeltaTime)
{
	if (SwarmIsScattering)
	{
		ScatteringTimer -= DeltaTime;

		if (ScatteringTimer <= 0.f)
			SwarmIsScattering = false;
	}
}