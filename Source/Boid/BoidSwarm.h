#pragma once

#include "Components/BoxComponent.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "BoidSwarm.generated.h"

class FBoid
{
	friend class ABoidSwarm;

public:
	FBoid() = default;
	FBoid(int NewIndex, FVector InitLocation, FVector InitVelocity)
		: Index(NewIndex), Location(InitLocation), Velocity(InitVelocity)
	{
	}

	void ApplyFutureValues(float DeltaTime)
	{
		Velocity = FutureVelocity;
		Location += Velocity * DeltaTime;
	}

	void UpdateInstancedMeshComponent(UInstancedStaticMeshComponent* ISMC, bool ReRenderAllInstances = false)
	{
		ISMC->UpdateInstanceTransform(Index, FTransform(Velocity.Rotation(), Location, FVector(1)), false, ReRenderAllInstances, false);
	}

	bool operator==(FBoid const& Other) const
	{
		return (Index == Other.Index);
	}

private:
	int Index;
	FVector Location;
	FVector Velocity;

	// Temporary variable in which the velocity for the next time step is stored.
	FVector FutureVelocity;
};

UCLASS()
class BOID_API ABoidSwarm : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh* BoidStaticMesh;

	// Total number of boids. Has to be capped for performance reasons.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 1, ClampMax = 10000))
	int StartingCount = 1500;

	// Minimal speed after applying all boid behaviour forces.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MinSpeed = 3000;

	// Maximal speed after applying all boid behaviour forces.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxSpeed = 5000;

	// Other boids in this radius are considered as close enough for alignment and cohesion.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PerceptionRadius = 2000;

	// Other boids in this radius are considered too close. Separation weight is applied to create space between.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AvoidanceRadius = 750;

	// Weight on boid alignment, i.e. the behaviour of facing in the same direction than surrounding others.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AlignmentWeight = 8000;

	// Weight on boid cohesian, i.e. the behaviour of moving towards surrounding others.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CohesionWeight = 10000;

	// Weight on boid separation, i.e. the behaviour of avoiding very close others.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SeparationWeight = 30000;

	// Weight which replaces the cohesian weight, when the swarm is in "scatter mode".
	// Negative weight makes the swarm separate. Positive weight can make it clump.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ScatteringWeight = 100000;

	// Outer box for the boids. Boids outside this box get an additional force to draw them back in.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* OuterLimitBox = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector OuterLimitBoxSize = FVector(5000);

	// Weight for the force that is applied to boids, when they leave the outer box bounds.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float StayInBoxWeight = 5000;

	// Inner box for the boids. Boids inside this box get an additional force to push them out.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* InnerLimitBox = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector InnerLimitBoxSize = FVector(500);

	// Weight for the force that is applied to boids, when they enter the inner box bounds.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float StayOutInnerBoxWeight = 10000;

	// Adds additional force in negative z direction.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0.f, ClampMax = 0.3f))
	float GravitationImpact = 0.02;

private:
	UInstancedStaticMeshComponent* BoidISMC;
	TArray<FBoid> Boids;
	FRandomStream RandomStream;
	bool SwarmIsScattering = false;
	float ScatteringTimer = 0;

public:
	ABoidSwarm();
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;

	virtual void Tick(float DeltaTime) override;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& Event) override;
#endif

	// Calling this function sets the internal "isScattering" state. Scattering behaviour is then applied.
	UFUNCTION(BlueprintCallable)
	void ScatterSwarm(float Duration);

private:
	void CountScatteringTime(float DeltaTime);

	void PrintFullSwarmInfo(FString const& Header);

	void SpawnStartingInstances();
	void CalcBoidVelocities(float DeltaTime);
	void MoveBoids(float DeltaTime);

	void CalcPerceivedSwarmStats(
		FBoid const& ProtagonistBoid, FVector& SwarmCenter, FVector& SwarmAvgVelocity, FVector& KeepDistanceDirection);
	FVector GetVectorFacingIntoBoxIfOutside(FBoid const& Boid);
	FVector GetVectorFacingOutOfBoxIfInside(FBoid const& Boid);

	void UpdateVisualBoxes();
};
