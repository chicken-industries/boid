using UnrealBuildTool;
using System.Collections.Generic;

public class BoidEditorTarget : TargetRules
{
	public BoidEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "Boid" } );
	}
}
