using UnrealBuildTool;
using System.Collections.Generic;

public class BoidTarget : TargetRules
{
	public BoidTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "Boid" } );
	}
}
