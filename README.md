# Boid Project

An UE4 implementation of the popular boid algorithm. Simulate the behaviour of a flock with thousands of entities.

## The Algorithm

The original boid algorithm is described as following. For every single boid we store a location (x) and a velocity (v). Gather data about sub flock in close distances. Then 3 virtual forces are applied on every timestep.

* Cohesian: Draw the boid towards the center of the perceived sub flock
* Separation: Push the boid away from very close others
* Alignment: Adjust velocity (speed + direction) to match the surrounding ones.

Additionally implemented:

* Scattering: An Event to temporarily adjust the cohesian force to scatter the flock (or pull it closer together for negative values).
* Soft blocking box: A force which is applied when bounds leave a bounding box.
* Gravity: Small adjustable constant force applied in negative z direction.

## Example Videos:
* [bird like](https://www.youtube.com/watch?v=EtiSR6N6CIA)
* [fish like](https://www.youtube.com/watch?v=SGqUfl63WQc)

## Planned Features

* Predators which hunt the original flock
* Path following and movable point of interest
* Obstacle avoiding

## Other Sources

Very nice polished video by Sebastian Lague.

* [Coding Adventure: Boids](https://www.youtube.com/watch?v=bqtqltqcQhw&t=381s)

Original paper of the algorithm

* [Craig Reynolds: Flocks, Herds, and Schools: A Distributed Behavioral Model](http://www.cs.toronto.edu/~dt/siggraph97-course/cwr87/)
